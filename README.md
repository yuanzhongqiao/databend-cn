<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><h1 align="center" tabindex="-1" dir="auto"><a id="user-content-databend-the-next-gen-cloud-dataai-analytics" class="anchor" aria-hidden="true" tabindex="-1" href="#databend-the-next-gen-cloud-dataai-analytics"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend：下一代云[数据+人工智能]分析</font></font></h1>
<div align="center" dir="auto">
<h4 align="center" tabindex="-1" dir="auto"><a id="user-content---databend-serverless-cloud-beta----documentation----benchmarking----roadmap-v13" class="anchor" aria-hidden="true" tabindex="-1" href="#--databend-serverless-cloud-beta----documentation----benchmarking----roadmap-v13"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>
  <a href="https://docs.databend.com/guides/cloud" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend 无服务器云（测试版）</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">   |
  </font></font><a href="https://docs.databend.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  |
  </font></font><a href="https://benchmark.clickhouse.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基准测试</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  |
  </font></font><a href="https://github.com/datafuselabs/databend/issues/11868" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/11868/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图 (v1.3)</font></font></a>
</h4>
<div dir="auto">
<a href="https://link.databend.rs/join-slack" rel="nofollow">
<img src="https://camo.githubusercontent.com/f253c6e18cddf9d955a5da9ce21467cdbfa76aa4471cb4ad02548f34c5387852/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f736c61636b2d6461746162656e642d3061626435393f6c6f676f3d736c61636b" alt="松弛" data-canonical-src="https://img.shields.io/badge/slack-databend-0abd59?logo=slack" style="max-width: 100%;">
</a>
<a href="https://link.databend.rs/join-feishu" rel="nofollow">
<img src="https://camo.githubusercontent.com/b40d6dc246ef0626276275986e36c88e3287df516d1ae28fd0b7e407b1088138/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6665697368752d6461746162656e642d306162643539" alt="肺术" data-canonical-src="https://img.shields.io/badge/feishu-databend-0abd59" style="max-width: 100%;">
</a>
<br>
<a href="https://github.com/datafuselabs/databend/actions/workflows/release.yml">
<img src="https://camo.githubusercontent.com/14ca0c2af9de7f2d8980bb587902ea6a454e735da1014b56801ac3a48df486eb/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f616374696f6e732f776f726b666c6f772f7374617475732f64617461667573656c6162732f6461746162656e642f72656c656173652e796d6c3f6272616e63683d6d61696e" alt="CI状态" data-canonical-src="https://img.shields.io/github/actions/workflow/status/datafuselabs/databend/release.yml?branch=main" style="max-width: 100%;">
</a>
<a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/6806c427021d8410d4961e57b7c3dae15b8d05a81dc23434c4c559c56e4d48f8/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f506c6174666f726d2d4c696e75782532432532306d61634f5325324325323041524d2d677265656e2e7376673f7374796c653d666c6174"><img src="https://camo.githubusercontent.com/6806c427021d8410d4961e57b7c3dae15b8d05a81dc23434c4c559c56e4d48f8/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f506c6174666f726d2d4c696e75782532432532306d61634f5325324325323041524d2d677265656e2e7376673f7374796c653d666c6174" alt="Linux平台" data-canonical-src="https://img.shields.io/badge/Platform-Linux%2C%20macOS%2C%20ARM-green.svg?style=flat" style="max-width: 100%;"></a>
</div>
</div>
<a target="_blank" rel="noopener noreferrer" href="https://private-user-images.githubusercontent.com/172204/279573083-9997d8bc-6462-4dbd-90e3-527cf50a709c.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU3NTE5NjYsIm5iZiI6MTcwNTc1MTY2NiwicGF0aCI6Ii8xNzIyMDQvMjc5NTczMDgzLTk5OTdkOGJjLTY0NjItNGRiZC05MGUzLTUyN2NmNTBhNzA5Yy5wbmc_WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTYmWC1BbXotQ3JlZGVudGlhbD1BS0lBVkNPRFlMU0E1M1BRSzRaQSUyRjIwMjQwMTIwJTJGdXMtZWFzdC0xJTJGczMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDI0MDEyMFQxMTU0MjZaJlgtQW16LUV4cGlyZXM9MzAwJlgtQW16LVNpZ25hdHVyZT1mMDgwMjlmN2U2ZTQ2ZmNlZDYxMjM4MzMzMmEyMDllZjBiYTE3YzQzNzk4ZjRkNDA2YjdmMjRmOTAwM2ZlYzZjJlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdCZhY3Rvcl9pZD0wJmtleV9pZD0wJnJlcG9faWQ9MCJ9.J2VEQQs0LUGoeOheDXjoE8iFLRvEJzIwvTyqQRiuRG8"><img src="https://private-user-images.githubusercontent.com/172204/279573083-9997d8bc-6462-4dbd-90e3-527cf50a709c.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU3NTE5NjYsIm5iZiI6MTcwNTc1MTY2NiwicGF0aCI6Ii8xNzIyMDQvMjc5NTczMDgzLTk5OTdkOGJjLTY0NjItNGRiZC05MGUzLTUyN2NmNTBhNzA5Yy5wbmc_WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTYmWC1BbXotQ3JlZGVudGlhbD1BS0lBVkNPRFlMU0E1M1BRSzRaQSUyRjIwMjQwMTIwJTJGdXMtZWFzdC0xJTJGczMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDI0MDEyMFQxMTU0MjZaJlgtQW16LUV4cGlyZXM9MzAwJlgtQW16LVNpZ25hdHVyZT1mMDgwMjlmN2U2ZTQ2ZmNlZDYxMjM4MzMzMmEyMDllZjBiYTE3YzQzNzk4ZjRkNDA2YjdmMjRmOTAwM2ZlYzZjJlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdCZhY3Rvcl9pZD0wJmtleV9pZD0wJnJlcG9faWQ9MCJ9.J2VEQQs0LUGoeOheDXjoE8iFLRvEJzIwvTyqQRiuRG8" alt="数据弯曲" style="max-width: 100%;"></a>
<h2 tabindex="-1" dir="auto"><a id="user-content--introduction" class="anchor" aria-hidden="true" tabindex="-1" href="#-introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🐋简介</font></font></h2>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个用 Rust 构建的开源、弹性和工作负载感知型云数据仓库，提供了</font></font><a href="https://github.com/datafuselabs/databend/issues/13059" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/13059/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Snowflake 的经济高效的替代方案</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">它专为对世界上最大的数据集进行复杂分析而设计。</font></font></p>
<h2 tabindex="-1" dir="auto"><a id="user-content--why-databend" class="anchor" aria-hidden="true" tabindex="-1" href="#-why-databend"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🚀 为什么选择 Databend</font></font></h2>
<ul dir="auto">
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">云友好</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：与各种云存储无缝集成，例如 AWS S3、Azure Blob、Google Cloud 等。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高性能</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：内置 Rust，利用 SIMD 和矢量化处理进行快速分析。</font></font><a href="https://databend.com/blog/clickbench-databend-top" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅 ClickBench</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">经济高效的弹性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：存储和计算分开扩展的创新设计，优化成本和性能。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">轻松的数据管理</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：摄取过程中的集成数据预处理消除了对外部 ETL 工具的需求。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据版本控制</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：提供类似Git的多版本存储，轻松实现任意时间点的数据查询、克隆和恢复。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">丰富的数据支持</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：处理多种数据格式和类型，包括 JSON、CSV、Parquet、ARRAY、TUPLE、MAP 和 JSON。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人工智能增强分析</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：提供具有集成</font></font><a href="https://docs.databend.com/sql/sql-functions/ai-functions/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人工智能功能的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高级分析功能。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">社区驱动</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：受益于友好且不断发展的社区，该社区为您的所有云分析提供易于使用的平台。</font></font></p>
</li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content--architecture" class="anchor" aria-hidden="true" tabindex="-1" href="#-architecture"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📐建筑</font></font></h2>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="https://private-user-images.githubusercontent.com/172204/279282759-68b1adc6-0ec1-41d4-9e1d-37b80ce0e5ef.jpg?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU3NTE5NjYsIm5iZiI6MTcwNTc1MTY2NiwicGF0aCI6Ii8xNzIyMDQvMjc5MjgyNzU5LTY4YjFhZGM2LTBlYzEtNDFkNC05ZTFkLTM3YjgwY2UwZTVlZi5qcGc_WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTYmWC1BbXotQ3JlZGVudGlhbD1BS0lBVkNPRFlMU0E1M1BRSzRaQSUyRjIwMjQwMTIwJTJGdXMtZWFzdC0xJTJGczMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDI0MDEyMFQxMTU0MjZaJlgtQW16LUV4cGlyZXM9MzAwJlgtQW16LVNpZ25hdHVyZT1mOThiZTM4YmI1ZTk5YmRiMjY1YTBmMzRlMTJiYmYwMGQ0ZWQ4ODQwOGFmZWRhMjEzYmNhZTE5ZDcxMjRlYjZjJlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdCZhY3Rvcl9pZD0wJmtleV9pZD0wJnJlcG9faWQ9MCJ9.v-8yWjLrDkQuQW_XNJoXnuyQynmSfiowJe48K29tPB8"><img src="https://private-user-images.githubusercontent.com/172204/279282759-68b1adc6-0ec1-41d4-9e1d-37b80ce0e5ef.jpg?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU3NTE5NjYsIm5iZiI6MTcwNTc1MTY2NiwicGF0aCI6Ii8xNzIyMDQvMjc5MjgyNzU5LTY4YjFhZGM2LTBlYzEtNDFkNC05ZTFkLTM3YjgwY2UwZTVlZi5qcGc_WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTYmWC1BbXotQ3JlZGVudGlhbD1BS0lBVkNPRFlMU0E1M1BRSzRaQSUyRjIwMjQwMTIwJTJGdXMtZWFzdC0xJTJGczMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDI0MDEyMFQxMTU0MjZaJlgtQW16LUV4cGlyZXM9MzAwJlgtQW16LVNpZ25hdHVyZT1mOThiZTM4YmI1ZTk5YmRiMjY1YTBmMzRlMTJiYmYwMGQ0ZWQ4ODQwOGFmZWRhMjEzYmNhZTE5ZDcxMjRlYjZjJlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdCZhY3Rvcl9pZD0wJmtleV9pZD0wJnJlcG9faWQ9MCJ9.v-8yWjLrDkQuQW_XNJoXnuyQynmSfiowJe48K29tPB8" alt="数据弯曲架构" style="max-width: 100%;"></a></p>
<h2 tabindex="-1" dir="auto"><a id="user-content--try-databend" class="anchor" aria-hidden="true" tabindex="-1" href="#-try-databend"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🚀 尝试数据弯曲</font></font></h2>
<h3 tabindex="-1" dir="auto"><a id="user-content-1-databend-serverless-cloud" class="anchor" aria-hidden="true" tabindex="-1" href="#1-databend-serverless-cloud"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.Databend无服务器云</font></font></h3>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">尝试 Databend 的最快方法，</font></font><a href="https://databend.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend Cloud</font></font></a></p>
<h3 tabindex="-1" dir="auto"><a id="user-content-2-install-databend-from-docker" class="anchor" aria-hidden="true" tabindex="-1" href="#2-install-databend-from-docker"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2. 从 Docker 安装 Databend</font></font></h3>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 Docker Hub 准备映像（一次）（这将下载大约 170 MB 的数据）：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker pull datafuselabs/databend</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="docker pull datafuselabs/databend" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要快速运行 Databend：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker run --net=host  datafuselabs/databend</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="docker run --net=host  datafuselabs/databend" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<h2 tabindex="-1" dir="auto"><a id="user-content--getting-started" class="anchor" aria-hidden="true" tabindex="-1" href="#-getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🚀 开始使用</font></font></h2>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署Databend</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/guides/deploy/understanding-deployment-modes" rel="nofollow">Understanding Deployment Modes</a></li>
<li><a href="https://docs.databend.com/guides/deploy/deploying-databend" rel="nofollow">Deploying a Standalone Databend</a></li>
<li><a href="https://docs.databend.com/guides/deploy/expanding-to-a-databend-cluster" rel="nofollow">Expanding a Standalone Databend</a></li>
<li><a href="https://docs.databend.com/guides/cloud" rel="nofollow">Databend Cloud (Beta)</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连接到 Databend</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/guides/sql-clients/bendsql" rel="nofollow">Connecting to Databend with BendSQL</a></li>
<li><a href="https://docs.databend.com/guides/sql-clients/jdbc" rel="nofollow">Connecting to Databend with JDBC</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将数据加载到 Databend</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/guides/load-data/load-semistructured/" rel="nofollow">Loading Semi-structured Data</a></li>
<li><a href="https://docs.databend.com/guides/load-data/transform/data-load-transform" rel="nofollow">Transforming Data During a Load</a></li>
<li><a href="https://docs.databend.com/guides/load-data/continuous-data-pipelines/" rel="nofollow">Continuous Data Pipelines</a></li>
<li><a href="https://docs.databend.com/guides/unload-data/" rel="nofollow">How to Unload Data from Databend</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Databend 加载数据工具</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/guides/load-data/load-db/kafka" rel="nofollow">Apache Kafka</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/airbyte" rel="nofollow">Airbyte</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/dbt" rel="nofollow">dbt</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/debezium" rel="nofollow">Debezium</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/flink-cdc" rel="nofollow">Apache Flink CDC</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/vector" rel="nofollow">DataDog Vector</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/addax" rel="nofollow">Addax</a></li>
<li><a href="https://docs.databend.com/guides/load-data/load-db/datax" rel="nofollow">DataX</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Databend 可视化工具</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/guides/visualize/deepnote" rel="nofollow">Deepnote</a></li>
<li><a href="https://docs.databend.com/guides/visualize/grafana" rel="nofollow">Grafana</a></li>
<li><a href="https://docs.databend.com/guides/visualize/jupyter" rel="nofollow">Jupyter Notebook</a></li>
<li><a href="https://docs.databend.com/guides/visualize/metabase" rel="nofollow">Metabase</a></li>
<li><a href="https://docs.databend.com/guides/visualize/mindsdb" rel="nofollow">MindsDB</a></li>
<li><a href="https://docs.databend.com/guides/visualize/redash" rel="nofollow">Redash</a></li>
<li><a href="https://docs.databend.com/guides/visualize/superset" rel="nofollow">Superset</a></li>
<li><a href="https://docs.databend.com/guides/visualize/tableau" rel="nofollow">Tableau</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理用户</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/user-create-user" rel="nofollow">How to Create a User</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/grant#granting-privileges" rel="nofollow">How to Grant Privileges to a User</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/revoke#revoking-privileges" rel="nofollow">How to Revoke Privileges from a User</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/user-create-role" rel="nofollow">How to Create a Role</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/grant#granting-role" rel="nofollow">How to Grant Privileges to a Role</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/grant" rel="nofollow">How to Grant Role to a User</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/user/revoke#revoking-role" rel="nofollow">How to Revoke the Role of a User</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理数据库</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/database/ddl-create-database" rel="nofollow">How to Create a Database</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/database/ddl-drop-database" rel="nofollow">How to Drop a Database</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理表</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/table/ddl-create-table" rel="nofollow">How to Create a Table</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/table/ddl-drop-table" rel="nofollow">How to Drop a Table</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/table/ddl-rename-table" rel="nofollow">How to Rename a Table</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/table/ddl-truncate-table" rel="nofollow">How to Truncate a Table</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/table/flashback-table" rel="nofollow">How to Flash Back a Table</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/table/alter-table-column" rel="nofollow">How to Add/Drop Table Column</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理数据</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/dml/dml-copy-into-table" rel="nofollow">COPY-INTO</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/dml/dml-insert" rel="nofollow">INSERT</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/dml/dml-delete-from" rel="nofollow">DELETE</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/dml/dml-update" rel="nofollow">UPDATE</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/dml/dml-replace" rel="nofollow">REPLACE</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/dml/dml-merge" rel="nofollow">MERGE-INTO</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理视图</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/view/ddl-create-view" rel="nofollow">How to Create a View</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/view/ddl-drop-view" rel="nofollow">How to Drop a View</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/view/ddl-alter-view" rel="nofollow">How to Alter a View</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人工智能功能</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-functions/ai-functions/ai-to-sql" rel="nofollow">Generating SQL with AI</a></li>
<li><a href="https://docs.databend.com/sql/sql-functions/ai-functions/ai-embedding-vector" rel="nofollow">Creating Embedding Vectors</a></li>
<li><a href="https://docs.databend.com/sql/sql-functions/ai-functions/ai-cosine-distance" rel="nofollow">Computing Text Similarities</a></li>
<li><a href="https://docs.databend.com/sql/sql-functions/ai-functions/ai-text-completion" rel="nofollow">Text Completion with AI</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据治理</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/mask-policy/create-mask-policy" rel="nofollow">How to Create Data Masking Policy</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/mask-policy/drop-mask-policy" rel="nofollow">How to Drop Data Masking Policy</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保护数据弯曲</font></font></summary>
<ul dir="auto">
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/network-policy/ddl-create-policy" rel="nofollow">How to Create Network Policy</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/network-policy/ddl-drop-policy" rel="nofollow">How to Drop Network Policy</a></li>
<li><a href="https://docs.databend.com/sql/sql-commands/ddl/network-policy/ddl-alter-policy" rel="nofollow">How to Alter Network Policy</a></li>
</ul>
</details>
<details>
<summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表现</font></font></summary>
<ul dir="auto">
<li><a href="https://databend.com/blog/clickbench-databend-top" rel="nofollow">Review Clickbench</a></li>
<li><a href="https://databend.com/blog/2022/08/08/benchmark-tpc-h" rel="nofollow">How to Benchmark Databend using TPC-H</a></li>
</ul>
</details>
<h2 tabindex="-1" dir="auto"><a id="user-content--contributing" class="anchor" aria-hidden="true" tabindex="-1" href="#-contributing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤝 贡献</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend 的蓬勃发展依赖于社区贡献！</font><font style="vertical-align: inherit;">无论是通过想法、代码还是文档，每一项努力都有助于增强我们的项目。</font><font style="vertical-align: inherit;">为了表示感谢，一旦您的代码被合并，您的名字将永远保留在</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">system.contributors</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表中。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下是一些可帮助您入门的资源：</font></font></p>
<ul dir="auto">
<li><a href="https://docs.databend.com/guides/overview/community/contributor/building-from-source" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码构建 Databend</font></font></a></li>
<li><a href="https://docs.databend.com/guides/overview/community/contributor/good-pr" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第一个好的 Pull 请求</font></font></a></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content--community" class="anchor" aria-hidden="true" tabindex="-1" href="#-community"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">👥 社区</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关使用 Databend 的指南，我们建议从官方文档开始。</font><font style="vertical-align: inherit;">如果您需要进一步帮助，请探索以下社区渠道：</font></font></p>
<ul dir="auto">
<li><a href="https://link.databend.rs/join-slack" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Slack</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于与社区进行实时讨论）</font></font></li>
<li><a href="https://github.com/datafuselabs/databend"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GitHub</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（功能/错误报告、贡献）</font></font></li>
<li><a href="https://twitter.com/DatabendLabs/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Twitter</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（快速获取新闻）</font></font></li>
<li><a href="https://link.databend.rs/i-m-feeling-lucky" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我感觉很幸运</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（现在就选一本好的第一期！）</font></font></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content-️-roadmap" class="anchor" aria-hidden="true" tabindex="-1" href="#️-roadmap"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🛣️路线图</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随时了解 Databend 的开发历程。</font><font style="vertical-align: inherit;">以下是我们路线图的里程碑：</font></font></p>
<ul dir="auto">
<li><a href="https://github.com/datafuselabs/databend/issues/14167" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/14167/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2024 年路线图</font></font></a></li>
<li><a href="https://github.com/datafuselabs/databend/issues/9448" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/9448/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2023 年路线图</font></font></a></li>
<li><a href="https://github.com/datafuselabs/databend/issues/11868" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/11868/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图 v1.3</font></font></a></li>
<li><a href="https://github.com/datafuselabs/databend/issues/11073" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/11073/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图 v1.2</font></font></a></li>
<li><a href="https://github.com/datafuselabs/databend/issues/10334" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/10334/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图 v1.1</font></font></a></li>
<li><a href="https://github.com/datafuselabs/databend/issues/9604" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/9604/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图 v1.0</font></font></a></li>
<li><a href="https://github.com/datafuselabs/databend/issues/7052" data-hovercard-type="issue" data-hovercard-url="/datafuselabs/databend/issues/7052/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图 v0.9</font></font></a></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content--license" class="anchor" aria-hidden="true" tabindex="-1" href="#-license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📜 许可证</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend 根据两个许可证的组合发布：</font></font><a href="/datafuselabs/databend/blob/main/licenses/Apache-2.0.txt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Apache License 2.0</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="/datafuselabs/databend/blob/main/licenses/Elastic.txt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Elastic License 2.0</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在向 Databend 做出贡献时，您可以在每个文件中找到相关的许可证标头。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关详细信息，请参阅</font></font><a href="/datafuselabs/databend/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">许可证</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件和</font></font><a href="https://docs.databend.com/guides/overview/editions/dee/license" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">许可常见问题解答</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<h2 tabindex="-1" dir="auto"><a id="user-content--acknowledgement" class="anchor" aria-hidden="true" tabindex="-1" href="#-acknowledgement"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🙏致谢</font></font></h2>
<ul dir="auto">
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">灵感</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Databend 的设计灵感来自行业领导者</font></font><a href="https://github.com/clickhouse/clickhouse"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ClickHouse</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://docs.snowflake.com/en/user-guide/intro-key-concepts.html#snowflake-architecture" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Snowflake</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">计算模型</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：我们的计算基础建立在</font></font><a href="https://github.com/jorgecarleitao/arrow2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Arrow2</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">之上，Arrow2 是 Apache Arrow 列格式的更快、更安全的版本。</font></font></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档托管</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font><a href="https://docs.databend.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Databend 文档网站自豪地在</font></font></a><font style="vertical-align: inherit;"></font><a href="https://vercel.com/?utm_source=databend&amp;utm_campaign=oss" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vercel</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上运行</font><font style="vertical-align: inherit;">。</font></font></p>
</li>
</ul>
</article></div>
